# PhotosPicker

PhotosPicker is an app that asks users to import a picture from their photo library, then attach a name to whatever they imported.

This app was developed as a challenge from the [100 Days of SwiftUI](https://www.hackingwithswift.com/guide/ios-swiftui/6/3/challenge) course from Hacking with Swift.