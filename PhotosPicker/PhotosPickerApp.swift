//
//  PhotosPickerApp.swift
//  PhotosPicker
//
//  Created by Pascal Hintze on 14.03.2024.
//

import SwiftData
import SwiftUI

@main
struct PhotosPickerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
        .modelContainer(for: Photo.self)
    }
}
