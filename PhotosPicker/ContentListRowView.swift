//
//  ContentListRowView.swift
//  PhotosPicker
//
//  Created by Pascal Hintze on 14.03.2024.
//

import MapKit
import SwiftUI

struct ContentListRowView: View {
    let photo: Photo
    
    var body: some View {
        NavigationStack {
            NavigationLink(value: photo) {
                HStack {
                    Image(uiImage: UIImage(data: photo.picture)!)
                        .resizable()
                        .scaledToFit()
                    
                    Spacer()
                    
                    Text(photo.name)
                        .font(.headline)
                        .foregroundStyle(.black)
                        .padding(.trailing, 20)
                }
            }
            .navigationDestination(for: Photo.self) { photo in
                DetailView(photo: photo)
            }
        }
        
    }
}

#Preview {
    ContentListRowView(photo: Photo(id: UUID(), location: nil, name: "Name", picture: Data()))
}
