//
//  Photo.swift
//  PhotosPicker
//
//  Created by Pascal Hintze on 14.03.2024.
//



import Foundation
import PhotosUI
import SwiftData

@Model
final class Photo: Identifiable {
    var id: UUID
    var location: Coordinate2D?
    var name: String
    @Attribute(.externalStorage) var picture: Data
    
    init(id: UUID, location: Coordinate2D?, name: String, picture: Data) {
        self.id = id
        self.location = location
        self.name = name
        self.picture = picture
    }
    
}
