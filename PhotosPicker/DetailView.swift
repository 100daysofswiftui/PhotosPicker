//
//  DetailView.swift
//  PhotosPicker
//
//  Created by Pascal Hintze on 14.03.2024.
//

import MapKit
import SwiftUI

struct DetailView: View {
    @Environment (\.dismiss) private var dismiss
    let photo: Photo
    @State private var showDisplayOnMapSheet = false
    
    
    var body: some View {
        VStack {
            Image(uiImage: UIImage(data: photo.picture)!)
                .resizable()
                .scaledToFill()
            Text(photo.name)
                .font(.title)
            Button("Display on Map") {
                showDisplayOnMapSheet.toggle()
            }
            .sheet(isPresented: $showDisplayOnMapSheet) {
                VStack {
                    if let coordinate = photo.location {
                        Map(initialPosition: .region(MKCoordinateRegion(
                            center: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude),
                            span: (MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))))) {
                                
                                Marker(photo.name, coordinate: CLLocationCoordinate2D(latitude: coordinate.latitude, longitude: coordinate.longitude))
                            }
                    } else {
                        ContentUnavailableView("No location",
                                               systemImage: "location.magnifyingglass",
                                               description: Text("No location was found for this image."))
                    }
                    Button("Dismiss") {
                        dismiss()
                    }
                }
                .padding()
            }
        }
        .padding()
    }
}

#Preview {
    DetailView(photo: Photo(id: UUID(), location: nil, name: "Name", picture: Data()))
}
