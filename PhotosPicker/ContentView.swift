//
//  ContentView.swift
//  PhotosPicker
//
//  Created by Pascal Hintze on 14.03.2024.
//

import PhotosUI
import SwiftData
import SwiftUI

struct ContentView: View {
    @Environment (\.modelContext) private var modelContext
    @State private var pickerItem: PhotosPickerItem?
    @State private var selectedPhoto: Photo?
    
    @State private var photoName = ""
    @State private var showSheet = false
    
    @Query(sort: \Photo.name) var selectedImages: [Photo]
    
    let locationFetcher = LocationFetcher()
    
    var bindingPhoto: Binding<Photo> {
        .init(get: { selectedPhoto ?? Photo(id: UUID(), location: nil, name: "", picture: Data())}, set: { selectedPhoto = $0 })
    }
    
    var body: some View {
        NavigationStack {
            VStack {
                List {
                    ForEach(selectedImages) { photo in
                        ContentListRowView(photo: photo)
                            .listRowSeparator(.visible)
                    }
                    .onDelete(perform: delete)
                    .frame(height: 100)
                }
                
            }
            
            PhotosPicker(selection: $pickerItem, matching: .images) {
                Label("Select a picture", systemImage: "photo")
            }
            .onChange(of: pickerItem, loadImage)
            .alert("Name", isPresented: $showSheet) {
                VStack {
                    TextField("Photo name", text: bindingPhoto.name, prompt: Text("Enter photo name"))
                    HStack {
                        Button("Cancel", role: .cancel) {
                            showSheet = false
                        }
                        Button("Done") {
                            if let newPhoto = selectedPhoto {
                                modelContext.insert(newPhoto)
                            }
                            showSheet = false
                        }
                    }
                    
                }
                .padding()
            }
        }
    }
    
    func loadImage() {
        Task {
            locationFetcher.start()
            guard let imageData = try await pickerItem?.loadTransferable(type: Data.self) else { return }
            guard let locationCoordinates = locationFetcher.lastKnownLocation else { return }
            selectedPhoto = Photo(id: UUID(),
                                  location: Coordinate2D(
                                    latitude: locationCoordinates.latitude, longitude: locationCoordinates.longitude),
                                  name: photoName, picture: imageData)
            showSheet.toggle()
            pickerItem = nil
        }
    }
    func delete(_ offsets: IndexSet) {
        for offset in offsets {
            let item = selectedImages[offset]
            modelContext.delete(item)
        }
    }
}

#Preview {
    ContentView()
}
